# mantis-bt

## __Purpose__

The purpose of this project is to ensure that you will not need to install a whole stack just to run the bug tracker.

### __3 Reasons why we should use this instead of building locally__

First of all, it's quick.  We do not need to install the whole entire XAMPP stack just to get the bug tracker up and running.

Second, this allows us to containerize our bug tracker so that it will not impact other applications that we might be running.

Lastly, we can bring the stack up and down whenever we want giving us the ability to save some resources.

## __How to use__

### __Initialize the stack__
In the root of this directory, start the stack using:

```bash
docker-compose up -d --build
```

The above command will build and bring up the services described in the `docker-compose.yml`.

### __Configuration__

Navigate to [the admin page at http://localhost:8989](http://localhost:8989).

You will need to populate the following fields:

1. hostname: this one is tricky, to start, try 172.22.0.2. If this fails, you will need to ssh into the container and find it by running `cat /etc/hosts`.
1. Username (for Database): root
1. Password (for Database): root
1. Admin Username (to create Database if required): mantisbt
1. Admin Password (to create Database if required): mantisbt

### __Removing the stack__

```bash
docker-compose down -v
```
